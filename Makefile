proxy:
	@cd ./Proxy && docker-compose --env-file ../.env up -d --build
build:
	@docker build -f ./Dockerfile/DockerfileNode -t devluismanuel/devops:latest .
init:
	@make proxy
	@docker-compose --env-file .env up -d --build
push:
	@docker image push devluismanuel/devops:latest